package analizador;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Analizador {

    int mensaje;
    ArrayList<String> texto;
    ArrayList<String> CEdeclaracion;
    ArrayList<String> CEMensajeE;
    ArrayList<String> VaStg;
    ArrayList<String> EsStg;
    ArrayList<String> VaEnt;
    ArrayList<String> EsEnt;
    ArrayList<String> VaFlt;
    ArrayList<String> EsFlt;
    ArrayList<String> CEMensajeTemp;
    int salto = 0;
    String varfor = "";
    int numEti = 0;

    public Analizador() {
        mensaje = 0;

        texto = new ArrayList<>();
        CEdeclaracion = new ArrayList<>();
        CEMensajeE = new ArrayList<>();
        VaEnt = new ArrayList<>();
        EsEnt = new ArrayList<>();
        VaFlt = new ArrayList<>();
        VaStg = new ArrayList<>();
        EsStg = new ArrayList<>();
        EsFlt = new ArrayList<>();

    }
    public static final String LRED = "\u001B[31m"; //letras de color rojo
    public static final String RESET = "\u001B[0m"; //leras de color negro (color original)
//    Metodos metodos = new Metodos();

//    ArrayList<String> reservada = metodos.getReservadas();
    public String analizadorLine(String token, int numLinea) {

        AnalizadorSintactico aa = new AnalizadorSintactico();
        Expresiones expresiones = new Expresiones();
        String[] palabras = token.split(" "); // se almacena en un arreglo las palabras 

        if (numLinea == 1 && !palabras[0].equals("INICIO;")) { //si en la primera linea no hay un INICIO;
            return LRED + "ERROR:" + RESET + "El programa no tiene un inicio";
        }

        switch (palabras[0]) {

            case "SD.DAT": //Case para mi salida de datos
                String expresion = expresiones.getSD();

                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el SD.DAT linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el SD linea: " + RESET + numLinea);

                } else {
                    try {
                        //SD.DAT ("HOLA MUNDO")
                        String texto = token.split("\"")[1];
                        CEdeclaracion.add("mov dx, offset Mensaje" + mensaje);
                        CEdeclaracion.add("mov ah, 09");
                        CEdeclaracion.add("int 21h ");
                        CEMensajeE.add("Mensaje" + mensaje + " db \"" + texto + "\",10,13,'$'");

                    } catch (Exception e) {

                        if ((!existevariable(token.split("\\(")[1].split("\\)")[0]))
                                && (!token.split("\\(")[1].split("\\)")[0].matches("[0-9]*(\\.[0-9]*){0,1}"))) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED
                                    + " La variable: " + RESET + token.split("\\(")[1].split("\\)")[0] + LRED + " no existe";

                        } else {

                            if (EsCadena(token.split("\\(")[1].split("\\)")[0])) {
                                //SD.DAT(Algo)

                                CEMensajeTemp = new ArrayList<>();
                                CEMensajeE.forEach((String data) -> {
                                    if (data.contains(token.split("\\(")[1].split("\\)")[0])) {
                                        data = data.replace("DW 10,13, '$'", "DB 11,0, 11 DUP (?)");
                                        CEMensajeTemp.add(data);
                                    } else {
                                        CEMensajeTemp.add(data);
                                    }
                                });
                                CEMensajeE = CEMensajeTemp;

                                CEdeclaracion.add("mov Dx, offset salto");
                                CEdeclaracion.add("mov ah, 09H ");
                                CEdeclaracion.add("int 21h ");

                                CEdeclaracion.add("mov ah,0Ah");
                                CEdeclaracion.add("mov Dx, offset " + token.split("\\(")[1].split("\\)")[0]);
                                CEdeclaracion.add("Push Dx");
                                CEdeclaracion.add("int 21h");

                                CEdeclaracion.add("mov dx, offset salto");
                                CEdeclaracion.add("mov ah, 9");
                                CEdeclaracion.add("int 21h");

                                CEdeclaracion.add("xor bx,bx");
                                CEdeclaracion.add("mov bl, " + token.split("\\(")[1].split("\\)")[0] + "[1]");
                                CEdeclaracion.add("mov " + token.split("\\(")[1].split("\\)")[0] + "[bx+2],'$'");

                                CEdeclaracion.add("mov Dx, offset " + token.split("\\(")[1].split("\\)")[0] + "+2");
                                CEdeclaracion.add("mov ah,9");
                                CEdeclaracion.add("int 21h");
                                /*     
                                CEdeclaracion.add("xor bx,bx ");
                                CEdeclaracion.add("mov bl, " + token.split("\\(")[1].split("\\)")[0] + "[1]");
                                CEdeclaracion.add("mov " + token.split("\\(")[1].split("\\)")[0] + " [bx+2],'$'");

                                CEdeclaracion.add("mov Dx, offset " + token.split("\\(")[1].split("\\)")[0] + " + 2");

                                CEdeclaracion.add("mov ah, 09H");
                                CEdeclaracion.add("int 21h ");*/

                                CEdeclaracion.add("mov Dx, offset salto");
                                CEdeclaracion.add("mov ah, 09H ");
                                CEdeclaracion.add("int 21h ");
                            } //fin if
                            if (EsEntero(token.split("\\(")[1].split("\\)")[0])) {
                                // SD.DAT(Variable)
                                CEdeclaracion.add("add " + token.split("\\(")[1].split("\\)")[0] + ", 30H");
                                CEdeclaracion.add("mov Dx, Offset " + token.split("\\(")[1].split("\\)")[0]);
                                CEdeclaracion.add("mov Ah, 09h");
                                CEdeclaracion.add("Int 21h");
                                CEdeclaracion.add("sub " + token.split("\\(")[1].split("\\)")[0] + ", 30H");

                            }
                            if (token.split("\\(")[1].split("\\)")[0].matches("[0-9]*")) {
                                //PARA CUANDO SE INGRESA SD.DAT (10)
                                CEdeclaracion.add("mov dx, offset Mensaje" + mensaje);
                                CEdeclaracion.add("mov ah, 09");
                                CEdeclaracion.add("int 21h ");
                                CEMensajeE.add("Mensaje" + mensaje + " db \"" + token.split("\\(")[1].split("\\)")[0] + "\",10,13,'$'");

                            } //fin if
                            if (token.split("\\(")[1].split("\\)")[0].matches("[0-9]*\\.[0-9]*")) {
                                //PARA CUANDO SE INGRESA UN SD.DAT(7.5)
                                CEdeclaracion.add("mov dx, offset Mensaje" + mensaje);
                                CEdeclaracion.add("mov ah, 09");
                                CEdeclaracion.add("int 21h ");
                                CEMensajeE.add("Mensaje" + mensaje + " db \"" + token.split("\\(")[1].split("\\)")[0] + "\",10,13,'$'");
                            } //fin if
                            if (EsFlt(token.split("\\(")[1].split("\\)")[0])) {

                                return LRED + "ERROR: en la linea: " + RESET + numLinea + LRED + " El token " + RESET + token.split("\\(")[1].split("\\)")[0] + LRED
                                        + " Es una variable tipo Flt y no soporta variables tipo decimal (Flotante)";

                            } //fin if
                        }// fin else
                    } //fin catch
                } //fin else
                break;

            case "ED.DAT": //case para mi entrada de datos
                expresion = expresiones.getED();

                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el ED linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el ED linea: " + RESET + numLinea);
                } else {
                    if (!existevariable(token.split("\\(")[1].split("\\)")[0])) {
                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED
                                + " La variable: " + RESET + token.split("\\(")[1].split("\\)")[0] + LRED + " no existe";
                        //System.out.println(LRED + "ERROR: La variable no existe"); 
                    } else {
                        CEdeclaracion.add("mov ah, 0Ah");  // Leer con 0AH
                        CEdeclaracion.add("mov Dx, Offset R1");
                        CEdeclaracion.add("int 21h");
                        CEdeclaracion.add("mov Si, offset R1+2"); //Pasar el valor a la variable que aparece en LEER
                        CEdeclaracion.add("mov Ah, 0");
                        CEdeclaracion.add("mov al, byte ptr [Si]");
                        CEdeclaracion.add("sub ax,30h");
                        CEdeclaracion.add("mov " + token.split("\\(")[1].split("\\)")[0] + ", ax"); //MOV A, AX
                        CEdeclaracion.add("mov Dx, Offset salto");  // Imprime salto para no encimar mensajes
                        CEdeclaracion.add("mov Ah, 9");             // Despues de que lee una variable
                        CEdeclaracion.add("Int 21h");


                        /* if (EsEntero(token.split("\\(")[1].split("\\)")[0])) {

                            CEdeclaracion.add("mov ah, 0Ah");
                            CEdeclaracion.add("mov Dx, Offset " + token.split("\\(")[1].split("\\)")[0]);

                            CEdeclaracion.add("Push Dx ");
                            CEdeclaracion.add("int 21h");

                            CEdeclaracion.add("mov Dx, offset salto");
                            CEdeclaracion.add("mov ah, 09H ");
                            CEdeclaracion.add("int 21h");

                            CEMensajeE.add(token.split("\\(")[1].split("\\)")[0] + " DB 3,0,3 DUP(?)");

                        }
                        if (EsCadena(token.split("\\(")[1].split("\\)")[0])) {

                            CEdeclaracion.add("mov ah, 0Ah");
                            CEdeclaracion.add("mov Dx, Offset " + token.split("\\(")[1].split("\\)")[0]);

                            CEdeclaracion.add("Push Dx ");
                            CEdeclaracion.add("int 21h");

                            CEdeclaracion.add("mov Dx, offset salto");
                            CEdeclaracion.add("mov ah, 09H ");
                            CEdeclaracion.add("int 21h");

                            CEMensajeE.add(token.split("\\(")[1].split("\\)")[0] + " DB " + "11,0,11 DUP('?'), '$'");

                        }
                        if (EsFlt(token.split("\\(")[1].split("\\)")[0])) {
                            return LRED + "ERROR: en la linea: " + RESET + numLinea + LRED + " El token " + RESET + token.split("\\(")[1].split("\\)")[0] + LRED
                                    + " Es una variable tipo Flt y no soporta variables tipo decimal (Flotante)";
                        }*/
                    }

                }
                break;

            case "Ent": //case para mi tipo de dato entero
                expresion = expresiones.getDecaEnt();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el Ent linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el Ent linea: " + RESET + numLinea);
                } else {
                    CEMensajeE.add(token.split(" ")[1] + " DW " + "0,10,13, '$'");
                    VaEnt.add(token.split(" ")[1]);
                    EsEnt.add(token.split(" ")[1]);

                }
                break;

            case "Flt": //case para mi tipo de dato decimal
                expresion = expresiones.getDecaFlt();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el Flt linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el Flt linea: " + RESET + numLinea);
                } else {
                    VaFlt.add(token.split(" ")[1]);
                    EsFlt.add(token.split(" ")[1]);
                    CEMensajeE.add(token.split(" ")[1] + " DB " + "0,10,13, '$'");
                }
                break;

            case "Stg": //case para mi tipo de dato de cadena
                expresion = expresiones.getDecaStr();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el Stg linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el Stg linea: " + RESET + numLinea);
                } else {

                    CEMensajeE.add(token.split(" ")[1] + " DW 10,13, '$'");
                    VaStg.add(token.split(" ")[1]);
                    EsStg.add(token.split(" ")[1]);
                }
                break;

            case "WH": //case para el while
                expresion = expresiones.getWH(); //Llamamos la expresion de mi while
                if (!estructura(expresion, token)) { //si no coincide con mi expresion definida
                    return LRED + "ERROR: en el WH linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el WH linea: " + RESET + numLinea);
                } else {
                    if (token.contains(">")) {
                        if ((!(existevariable(token.split("\\:")[1].split("\\>")[0]) || token.split("\\:")[1].split("\\>")[0].matches("[0-9]*"))
                                || !(existevariable(token.split("\\:")[1].split("\\>")[1]) || token.split("\\:")[1].split("\\>")[1].matches("[0-9]*")))) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                        } else {
                            CEdeclaracion.add("Push Ax");
                            CEdeclaracion.add("Push Bx");
                            CEdeclaracion.add("ini_WH" + numEti + ":");
                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\>")[0]);
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\>")[1]);
                            CEdeclaracion.add("cmp Ax, Bx, ");
                            CEdeclaracion.add("jna fin_WH" + numEti);
                        }
                    }
                    if (token.contains("<")) {
                        if (!(existevariable(token.split("\\:")[1].split("\\<")[0]) || token.split("\\:")[1].split("\\<")[0].matches("[0-9]*"))
                                || !(existevariable(token.split("\\:")[1].split("\\<")[1]) || token.split("\\:")[1].split("\\<")[1].matches("[0-9]*"))) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                        } else {
                            CEdeclaracion.add("Push Ax");
                            CEdeclaracion.add("Push Bx");
                            CEdeclaracion.add("ini_WH" + numEti + ":");
                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\<")[0]);
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\<")[1]);
                            CEdeclaracion.add("cmp Ax, Bx, ");
                            CEdeclaracion.add("jnb fin_WH" + numEti);
                        }
                    }
                    if (token.contains("==")) {
                        if (!(existevariable(token.split("\\:")[1].split("\\==")[0]) || token.split("\\:")[1].split("\\==")[0].matches("[0-9]*"))
                                || !(existevariable(token.split("\\:")[1].split("\\==")[1]) || token.split("\\:")[1].split("\\==")[1].matches("[0-9]*"))) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                        } else {
                            CEdeclaracion.add("Push Ax");
                            CEdeclaracion.add("Push Bx");
                            CEdeclaracion.add("ini_WH" + numEti + ":");
                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\==")[0]);
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\==")[1]);
                            CEdeclaracion.add("cmp Ax, Bx, ");
                            CEdeclaracion.add("je fin_WH" + numEti);
                        }
                    }
                    if (token.contains("!=")) {
                        if (!(existevariable(token.split("\\:")[1].split("\\!=")[0]) || token.split("\\:")[1].split("\\!=")[0].matches("[0-9]*"))
                                || !(existevariable(token.split("\\:")[1].split("\\!=")[1]) || token.split("\\:")[1].split("\\!=")[1].matches("[0-9]*"))) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                        } else {
                            CEdeclaracion.add("Push Ax");
                            CEdeclaracion.add("Push Bx");
                            CEdeclaracion.add("ini_WH" + numEti + ":");
                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\!=")[0]);
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\!=")[1]);
                            CEdeclaracion.add("cmp Ax, Bx, ");
                            CEdeclaracion.add("jne fin_WH" + numEti);
                        }
                    }

                    return "ABIERTOWH";

                }

            case "Fin_WH":
                expresion = expresiones.getCiclosFin();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el Fin_WH linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el Stg linea: " + RESET + numLinea);
                } else {

                    CEdeclaracion.add("jmp ini_WH" + numEti);
                    CEdeclaracion.add("fin_WH" + numEti + ":");
                    CEdeclaracion.add("pop BX");
                    CEdeclaracion.add("pop AX");
                    numEti++;
                }
                return "CERRARWH";

            case "FR":
                expresion = expresiones.getFr();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el FR linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el FR linea: " + RESET + numLinea);
                } else {
                    if (!existevariable(token.split(" ")[1])) {
                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                        //System.out.println(LRED + "ERROR: La variable no existe"); 
                    } else {
                        //FR Num1 :Desde(3)Hasta(4):
                        varfor = token.split(" ")[1];
                        CEdeclaracion.add("mov " + varfor + ", " + token.split("\\(")[1].split("\\)")[0]);
                        CEdeclaracion.add("mov Cx, " + token.split("\\(")[2].split("\\)")[0]);
                        CEdeclaracion.add("sub Cx, " + token.split("\\(")[1].split("\\)")[0]);
                        CEdeclaracion.add("add Cx, " + 1);
                        CEdeclaracion.add("eti" + numEti + ":");
                    }
                    return "ABIERTOFOR";

                }

            case "Fin_For":
                expresion = expresiones.getCiclosFin();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el Fin_For linea: " + RESET + numLinea;
                    //System.out.println(LRED + "ERROR: en el Stg linea: " + RESET + numLinea);
                } else {
                    CEdeclaracion.add("add " + varfor + ", 1");
                    CEdeclaracion.add("Loop eti" + numEti);
                    numEti++;
                }
                return "CERRARFOR";

            case "Si": //case para mi if
                expresion = expresiones.getSi();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en el Si linea: " + RESET + numLinea;
                    // System.out.println(LRED + "ERROR: en el Si linea: " + RESET + numLinea);
                } else {
                    //Validaciones donde verifica si existe la variable que esta en la condicion o si son numeros enteros por ejemplo
                    //Si :A>2:{  || Si :A<B:{ etc etc
                    if ((!(existevariable(token.split("\\:")[1].split("\\>")[0]) || token.split("\\:")[1].split("\\>")[0].matches("[0-9]*"))
                            || !(existevariable(token.split("\\:")[1].split("\\>")[1]) || token.split("\\:")[1].split("\\>")[1].matches("[0-9]*")))
                            && (!(existevariable(token.split("\\:")[1].split("\\<")[0]) || token.split("\\:")[1].split("\\<")[0].matches("[0-9]*"))
                            || !(existevariable(token.split("\\:")[1].split("\\<")[1]) || token.split("\\:")[1].split("\\<")[1].matches("[0-9]*")))
                            && (!(existevariable(token.split("\\:")[1].split("\\==")[0]) || token.split("\\:")[1].split("\\==")[0].matches("[0-9]*"))
                            || !(existevariable(token.split("\\:")[1].split("\\==")[1]) || token.split("\\:")[1].split("\\==")[1].matches("[0-9]*")))
                            && (!(existevariable(token.split("\\:")[1].split("\\!=")[0]) || token.split("\\:")[1].split("\\!=")[0].matches("[0-9]*"))
                            || !(existevariable(token.split("\\:")[1].split("\\!=")[1]) || token.split("\\:")[1].split("\\!=")[1].matches("[0-9]*")))) {
                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                        //System.out.println(LRED + "ERROR: La variable no existe"); 
                    } else {
                        if (token.contains(">")) {

                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\>")[0]); // Pasa a AX  la variables 
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\>")[1]); // Pasa a BX la variable 
                            CEdeclaracion.add("CMP AX,BX");
                            CEdeclaracion.add("jna salto" + salto);//etiqueta

                        } else if (token.contains("<")) {

                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\<")[0]); // Pasa a AX  la variables 
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\<")[1]); // Pasa a BX la variable 
                            CEdeclaracion.add("CMP AX,BX");
                            CEdeclaracion.add("jnb salto" + salto);//etiqueta

                        } else if (token.contains("==")) {

                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\==")[0]); // Pasa a AX  la variables 
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\==")[1]); // Pasa a BX la variable 
                            CEdeclaracion.add("CMP AX,BX");
                            CEdeclaracion.add("je salto" + salto);//etiqueta
                        } else if (token.contains("!=")) {

                            CEdeclaracion.add("mov Ax, " + token.split("\\:")[1].split("\\!=")[0]); // Pasa a AX  la variables 
                            CEdeclaracion.add("mov Bx, " + token.split("\\:")[1].split("\\!=")[1]); // Pasa a BX la variable 
                            CEdeclaracion.add("CMP AX,BX");
                            CEdeclaracion.add("jne salto" + salto);//etiqueta

                        }

                    }
                    return "ABIERTO";
                }

            case "}": //case para si se olvido cerrar una llave

                CEdeclaracion.add("salto" + salto + ":");
                salto++;

                return "CERRAR";
            case "INICIO;": //case para si hay mas de un INICIO; En mi programa
                if (numLinea != 1) {
                    return LRED + "ERROR: En la linea " + numLinea + RESET + " Ya contiene un inicio el programa";
                }
                break;
            case "FIN;": //Determinar el final de mi programa
                return "FIN";
            default: //Si no tengo una palabra definida, entran aqui para ser validadas
                expresion = expresiones.getExp();
                String Asignacion = expresiones.getAsigEXPE();
                String AsigSimple = expresiones.getAsigVar();
                if (!estructura(expresion, token)) {
                    return LRED + "ERROR: en la linea: " + RESET + numLinea + LRED + " Con el token: " + RESET + token;
                    //System.out.println(LRED + "ERROR: en la linea: " + RESET + numLinea + LRED + " Con el token: " + RESET + token);

                } else {
                    if (token.matches(AsigSimple)) { //Asigancion simple: NUM = NUM2
                        if (!existevariable(token.split(" ")[0])) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable: " + RESET + token.split(" ")[0] + LRED + " no existe";

                        } else {
                            //Num1 = Num2
                            CEdeclaracion.add("mov Ax," + token.split("\\=")[1]); //****
                            CEdeclaracion.add("mov " + token.split("\\=")[0] + ", Ax");

                            /* CEdeclaracion.add("mov ah, 0");
                            CEdeclaracion.add("mov al, byte ptr [Si]");
                            CEdeclaracion.add("PUSH AX");*/
                        }
                    }
                    if (token.matches(Asignacion)) { //
                        //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ARITMETICAS * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                        if (!existevariable(token.split(" ")[0])) {
                            return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable: " + RESET + token.split(" ")[0] + LRED + " no existe";
                            //System.out.println(LRED + "ERROR: La variable no existe"); 
                        } else {
                            if (token.contains("-")) {
                                //A = B-C
                                //A = B-2
                                if (!(existevariable(token.split(" ")[2].split("\\-")[0]) || token.split(" ")[2].split("\\-")[0].matches("[0-9]*"))
                                        || !(existevariable(token.split(" ")[2].split("\\-")[1]) || token.split(" ")[2].split("\\-")[1].matches("[0-9]*"))) { //Validacion si existen las variables A = N-B
                                    return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                                } else {
                                    if (EsCadena(token.split(" ")[0]) && EsCadena(token.split(" ")[2].split("\\-")[0]) && EsCadena(token.split(" ")[2].split("\\-")[1])) { //Validacion de variable Stg
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " No se puede generar una resta de cadenas";

                                    }
                                    if (EsFlt(token.split(" ")[0]) && EsFlt(token.split(" ")[2].split("\\-")[0]) && EsFlt(token.split(" ")[2].split("\\-")[1])) {
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " Por el moemnto no se puede generar una resta de flotantes";

                                    }
                                    if (EsEntero(token.split(" ")[0]) && (EsEntero(token.split(" ")[2].split("\\-")[0]) || token.split(" ")[2].split("\\-")[0].matches("[0-9]*"))
                                            && (EsEntero(token.split(" ")[2].split("\\-")[1]) || token.split(" ")[2].split("\\-")[1].matches("[0-9]*"))) {

                                        CEdeclaracion.add("mov ax, " + token.split(" ")[2].split("\\-")[0]);// AX toma el valor de A o primer opdo
                                        CEdeclaracion.add("mov bx, " + token.split(" ")[2].split("\\-")[1]);// BX toma el valor de B o segundo opdo
                                        CEdeclaracion.add("sub ax, bx");       // Efectua la resta
                                        CEdeclaracion.add("mov " + token.split(" ")[0] + ", ax");  // Mueve resultado a C o variable
                                    } else {
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " Las variables tienen que ser del mismo tipo";
                                    }
                                } //fin del else
                            } else if (token.contains("+")) {
                                // A = B+C
                                // A = B+2
                                if (!(existevariable(token.split(" ")[2].split("\\+")[0]) || token.split(" ")[2].split("\\+")[0].matches("[0-9]*"))
                                        || !(existevariable(token.split(" ")[2].split("\\+")[1]) || token.split(" ")[2].split("\\+")[1].matches("[0-9]*"))) { //Validacion si existen las variables A = N+B
                                    return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                                } else {
                                    if (EsCadena(token.split(" ")[0]) && EsCadena(token.split(" ")[2].split("\\+")[0]) && EsCadena(token.split(" ")[2].split("\\+")[1])) { //Validacion de variable Stg
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " No se puede generar una suma de cadenas";

                                    }
                                    if (EsFlt(token.split(" ")[0]) && EsFlt(token.split(" ")[2].split("\\+")[0]) && EsFlt(token.split(" ")[2].split("\\+")[1])) {
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " Por el moemnto no se puede generar una suma de flotantes";

                                    }
                                    if (EsEntero(token.split(" ")[0]) && (EsEntero(token.split(" ")[2].split("\\+")[0]) || token.split(" ")[2].split("\\+")[0].matches("[0-9]*"))
                                            && (EsEntero(token.split(" ")[2].split("\\+")[1]) || token.split(" ")[2].split("\\+")[1].matches("[0-9]*"))) {

                                        CEdeclaracion.add("mov ax, " + token.split(" ")[2].split("\\+")[0]);// AX toma el valor de A o primer opdo
                                        CEdeclaracion.add("mov bx, " + token.split(" ")[2].split("\\+")[1]);// BX toma el valor de B o segundo opdo
                                        CEdeclaracion.add("add ax, bx");       // Efectua suma
                                        CEdeclaracion.add("mov " + token.split(" ")[0] + ", ax");  // Mueve resultado a C o variable
                                    } else {
                                        return LRED + "ERROR: En la linea " + numLinea + RESET + " Las variables tienen que ser del mismo tipo";
                                    }
                                }//fin else
                            } else if (token.contains("*")) {
                                //A = B*C
                                //A = B*2
                                if (!(existevariable(token.split(" ")[2].split("\\*")[0]) || token.split(" ")[2].split("\\*")[0].matches("[0-9]*"))
                                        || !(existevariable(token.split(" ")[2].split("\\*")[1]) || token.split(" ")[2].split("\\*")[1].matches("[0-9]*"))) { //Validacion si existen las variables A = N*B
                                    return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                                } else {
                                    if (EsCadena(token.split(" ")[0]) && EsCadena(token.split(" ")[2].split("\\*")[0]) && EsCadena(token.split(" ")[2].split("\\*")[1])) { //Validacion de variable Stg
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " No se puede generar una multiplicacion de cadenas";

                                    }
                                    if (EsFlt(token.split(" ")[0]) && EsFlt(token.split(" ")[2].split("\\*")[0]) && EsFlt(token.split(" ")[2].split("\\*")[1])) {
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " Por el moemnto no se puede generar una multiplicacion de flotantes";

                                    }
                                    if (EsEntero(token.split(" ")[0]) && (EsEntero(token.split(" ")[2].split("\\*")[0]) || token.split(" ")[2].split("\\*")[0].matches("[0-9]*"))
                                            && (EsEntero(token.split(" ")[2].split("\\*")[1]) || token.split(" ")[2].split("\\*")[1].matches("[0-9]*"))) {

                                        CEdeclaracion.add("mov ax, " + token.split(" ")[2].split("\\*")[0]);// AX toma el valor de A o primer opdo
                                        CEdeclaracion.add("mov bx, " + token.split(" ")[2].split("\\*")[1]);// BX toma el valor de B o segundo opdo
                                        //CEdeclaracion.add("sub ax, 30H");
                                        //CEdeclaracion.add("add bx, 30H");
                                        // CEdeclaracion.add("mul " + token.split(" ")[2].split("\\*")[1]);  //Efectuamos la multiplicacion
                                        CEdeclaracion.add("mul bx");  //Efectuamos la multiplicacion

                                        // CEdeclaracion.add("add ax, 30H");
                                        // CEdeclaracion.add("sub ax, 60H");//AX = AX + 30 suma 30 hexadecimal para que coincida con el símbolo del dígito correspondiente       
                                        CEdeclaracion.add("mov " + token.split(" ")[0] + ", ax");  // Mueve resultado a la variable

                                    } else {
                                        return LRED + "ERROR: En la linea " + numLinea + RESET + " Las variables tienen que ser del mismo tipo";
                                    }
                                }//fin else
                            } else if (token.contains("DV")) {
                                //A = BDVC
                                //A = BDV2
                                if (!(existevariable(token.split(" ")[2].split("\\DV")[0]) || token.split(" ")[2].split("\\DV")[0].matches("[0-9]*"))
                                        || !(existevariable(token.split(" ")[2].split("\\DV")[1]) || token.split(" ")[2].split("\\DV")[1].matches("[0-9]*"))) { //Validacion si existen las variables A = NDVB
                                    return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                                } else {
                                    if (EsCadena(token.split(" ")[0]) && EsCadena(token.split(" ")[2].split("\\DV")[0]) && EsCadena(token.split(" ")[2].split("\\DV")[1])) { //Validacion de variable Stg
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " No se puede generar una division entera de cadenas";

                                    }
                                    if (EsFlt(token.split(" ")[0]) && EsFlt(token.split(" ")[2].split("\\DV")[0]) && EsFlt(token.split(" ")[2].split("\\DV")[1])) {
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " Por el moemnto no se puede generar una division entera de flotantes";

                                    }
                                    if (EsEntero(token.split(" ")[0]) && (EsEntero(token.split(" ")[2].split("\\DV")[0]) || token.split(" ")[2].split("\\DV")[0].matches("[0-9]*"))
                                            && (EsEntero(token.split(" ")[2].split("\\DV")[1]) || token.split(" ")[2].split("\\DV")[1].matches("[0-9]*"))) {

                                        CEMensajeTemp = new ArrayList<>();
                                        CEMensajeE.forEach((String data) -> {
                                            if (data.contains(token.split(" ")[0])) {
                                                data = data.replace("DW", "DB");
                                                CEMensajeTemp.add(data);
                                            } else {
                                                CEMensajeTemp.add(data);
                                            }
                                        });
                                        CEMensajeE = CEMensajeTemp;
                                        CEdeclaracion.add("mov ax, " + token.split(" ")[2].split("\\DV")[0]);// AX toma el valor de A o primer opdo
                                        CEdeclaracion.add("mov bx, " + token.split(" ")[2].split("\\DV")[1]);// BX toma el valor de B o segundo opdo
                                        CEdeclaracion.add("div BL");  //Efectuamos la division
                                        CEdeclaracion.add("mov " + token.split(" ")[0] + ", AL");  // Mueve resultado a la variable

                                    } else {
                                        return LRED + "ERROR: En la linea " + numLinea + RESET + " Las variables tienen que ser del mismo tipo";
                                    }
                                }//fin else
                            } else if (token.contains("%")) {
                                //A = B%C
                                //A = B%2
                                if (!(existevariable(token.split(" ")[2].split("\\%")[0]) || token.split(" ")[2].split("\\%")[0].matches("[0-9]*"))
                                        || !(existevariable(token.split(" ")[2].split("\\%")[1]) || token.split(" ")[2].split("\\%")[1].matches("[0-9]*"))) { //Validacion si existen las variables A = N-B
                                    return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " La variable no existe";
                                } else {
                                    if (EsCadena(token.split(" ")[0]) && EsCadena(token.split(" ")[2].split("\\%")[0]) && EsCadena(token.split(" ")[2].split("\\%")[1])) { //Validacion de variable Stg
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " No se puede generar el residuo de cadenas";

                                    }
                                    if (EsFlt(token.split(" ")[0]) && EsFlt(token.split(" ")[2].split("\\%")[0]) && EsFlt(token.split(" ")[2].split("\\%")[1])) {
                                        return LRED + "ERROR: En la linea " + RESET + numLinea + LRED + " Por el moemnto no se puede generar una el residuo de una division de flotantes";

                                    }
                                    if (EsEntero(token.split(" ")[0]) && (EsEntero(token.split(" ")[2].split("\\%")[0]) || token.split(" ")[2].split("\\%")[0].matches("[0-9]*"))
                                            && (EsEntero(token.split(" ")[2].split("\\%")[1]) || token.split(" ")[2].split("\\%")[1].matches("[0-9]*"))) {

                                        CEMensajeTemp = new ArrayList<>();
                                        CEMensajeE.forEach((String data) -> { //recorre el arrego 
                                            if (data.contains(token.split(" ")[0])) { //si encuentra algo antes del = (A = BDVC) en este caso seria A
                                                data = data.replace("DW", "DB"); //Reemplaza lo que tenga en DW como DB para la variable
                                                CEMensajeTemp.add(data); //Se añade a la lista temporal lo que se reemplazo
                                            } else {
                                                CEMensajeTemp.add(data); //Si no encuentra no reemplaza nada
                                            }
                                        });
                                        CEMensajeE = CEMensajeTemp; //pasamos a la lista buena lo que hay en la temporal.
                                        CEdeclaracion.add("mov ax, " + token.split(" ")[2].split("\\%")[0]);// AX toma el valor de A o primer opdo
                                        CEdeclaracion.add("mov bx, " + token.split(" ")[2].split("\\%")[1]);// BX toma el valor de B o segundo opdo
                                        CEdeclaracion.add("div BL");  //Efectuamos la divison pero el residuo se almacena en bl
                                        CEdeclaracion.add("mov " + token.split(" ")[0] + ", AH");  // Mueve resultado a la variable

                                    } else {
                                        return LRED + "ERROR: En la linea " + numLinea + RESET + "Laa variables tienen que ser del mismo tipo";
                                    }

                                }//fin else 
                            }

                        }
                    }

                }
                break;
        }

        return "";
    }

    //Metodo donde se valida la estructura de mis expresiones
    public boolean estructura(String exp, String token) {
        Pattern patron = Pattern.compile(exp);
        Matcher mcher = patron.matcher(token);
        return mcher.matches();
    }

    public void addContador() {
        this.mensaje++;
    }

    public void addLista(String palabra) {
        this.texto.add(palabra);
    }

    public ArrayList<String> Declaraciones() { //retorna la lista
        return CEdeclaracion;
    }

    public boolean EsEntero(String var) {
        for (Object Variable : EsEnt) {
            if (Variable.equals(var)) {
                return true;
            }
        }
        return false;
    }

    public boolean EsCadena(String var) {
        for (Object Variable : EsStg) {
            if (Variable.equals(var)) {
                return true;
            }
        }
        return false;
    }

    public boolean EsFlt(String var) {
        for (Object Variable : EsFlt) {
            if (Variable.equals(var)) {
                return true;
            }
        }
        return false;
    }

    public boolean existevariable(String var) {
        for (String variable : VaEnt) {
            if (variable.equals(var)) {
                return true;
            }
        }
        for (String variable : VaFlt) {
            if (variable.equals(var)) {
                return true;
            }
        }
        for (String variable : VaStg) {
            if (variable.equals(var)) {
                return true;
            }
        }

        return false;
    }

    // public boolean YaExiste (String var)
}

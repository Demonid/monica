package analizador;

import java.util.ArrayList;

public class AnalizadorSintactico {

    public static final String LRED = "\u001B[31m"; //letras de color rojo
    public static final String RESET = "\u001B[0m"; //leras de color negro (color original)
    public static final String LP = "\u001B[35m"; //leras de color morado 

    public static Analizador analiz = new Analizador();
    public static Metodos metodos = new Metodos();
    public static ArrayList<String> Errores = new ArrayList<String>();
    public static ArrayList<String> codigo = metodos.getCodigo();

    public static void main(String[] args) {

        Boolean fin = false;

        System.out.println(LP + "NOTA: Mis variables tienen que ser 2 letras como minimo y siempre empezando con una mayuscula");
//        ArrayList<String> reservada = metodos.getReservadas();

        int llave = 0;
        int numLinea = 1;
        int llaveWH = 0;
        int llavefor = 0;
        for (String linea : codigo) {
            //se gurda es resp lo obtenido de llamar el metodo para despues comparar 

            if (linea.contains("SD.DAT")) {
                analiz.addContador(); //contador para mis mensajes en la salida de dtos
            }
            if (linea.contains("ED.DAT")) {
                analiz.addContador(); //contador para mis mensajes en la salida de dtos
            }
            String resp = analiz.analizadorLine(linea, numLinea);
            //Si hay un "ERROR:"  el error se almacena en la lista de errores
            if (resp.contains("ERROR:")) {
                Errores.add(resp);
            }
            //Para el conteo de las llaves abiertas y cerradas
            if (resp.equals("ABIERTO")) {
                llave++;
            }
            if (resp.equals("CERRAR")) {
                llave--;
            }
            //Para el ciclo While su respeciva "llave"
            if (resp.equals("ABIERTOWH")) {
                llaveWH++;
            }
            if (resp.equals("CERRARWH")) {
                llaveWH--;
            }
            //Para el ciclo For su respeciva "llave"
            if (resp.equals("ABIERTOFOR")) {
                llavefor++;
            }
            if (resp.equals("CERRARFOR")) {
                llavefor--;
            }
            if (fin) {
                Errores.add(LRED + "ERROR: " + RESET + "No puedes escribir codigo despues del fin");
                break;
            }
            if (resp.equals("FIN")) {
                fin = true;
            }
            numLinea++;
        }
        //Validar si falto cerrar una llave o si se añadio de mas.
        if (llave > 0) {
            Errores.add(LRED + "ERROR:" + RESET + " Falto cerrar una llave");
        }
        if (llave < 0) {
            Errores.add(LRED + "ERROR:" + RESET + " Agrego una llave de más");

        }
        //Se valida si esta el respectivo "Fin_WH"
        if (llaveWH > 0) {
            Errores.add(LRED + "ERROR:" + RESET + " Falto cerrar con 'Fin_WH'");
        }
        if (llaveWH < 0) { //Valida si agrego un "Fin_WH" de mas
            Errores.add(LRED + "ERROR:" + RESET + " Agrego un 'Fin_WH' de mas");

        }
        if (llavefor > 0) { //Se valida si esta el respectivo "Fin_For"
            Errores.add(LRED + "ERROR:" + RESET + " Falto cerrar con 'Fin_For'");
        }
        if (llavefor < 0) { //Valida si agrego un "Fin_For" de mas
            Errores.add(LRED + "ERROR:" + RESET + " Agrego un 'Fin_For' de mas");

        }
        if (!fin) {
            Errores.add(LRED + "ERROR: " + RESET + "Falto el fin del programa");
        }

        if (Errores.size() > 0) { //Si hay errores, los imprime
            for (String error : Errores) {
                System.out.println(error);
            }
        } else {

            System.out.println(".MODEL SMALL");
            System.out.println(".CODE");
            System.out.println("INICIO:");
            System.out.println("mov Ax, @Data");
            System.out.println("mov Ds, Ax");

            for (String msj : analiz.CEdeclaracion) {
                System.out.println(msj);
            }

            System.out.println("Mov Ax, 4C00H\nINT 21H\n.DATA");
            System.out.println("R1 DB 3, 0, 3 DUP(?)");
            System.out.println("salto db 10,13,10,13,'$'");
            for (String msj : analiz.CEMensajeE) {
                System.out.println(msj);
            }

            System.out.println(".Stack");
            System.out.println("END INICIO");
        }
    }
}

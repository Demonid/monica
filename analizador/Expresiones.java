package analizador;

public class Expresiones {

    private String Variable;
    private String WH;
    private String SD;
    private String ED;
    private String Si;
    private String Fr;
    private String AsigE;
    private String AsigF;
    private String AsigC;
    private String Tipo;
    private String OpLog;
    private String OPA;
    private String ConstOva;
    private String DecaEnt;
    private String DecaStr;
    private String DecaFlt;
    private String exp;
    private String OpRel;
    private String Comp;
    private String Num;
    private String finExp;
    private String ConstF;
    private String AsigSimple;
    private String AsigEXPE;
    private String AsigEXPF;
    private String AsigEXPC;
    private String EspIG;
    private String expAriE;
    private String expAriF;
    private String expAriC;
    private String ConstC;
    private String AsigVar;
    private String CiclosFin;

    //Expresiones regulares de todas mis sentencias
    public Expresiones() {
        OpLog = "(\\&&|[v]{2})";
        finExp = "[\\s]*";
        EspIG = "([\\s]*\\=[\\s]*)";
        OpRel = "(\\<|\\>|\\==|\\<=|\\>=|\\!=)";
        Tipo = "Ent|Flt|Stg";
        Num = "[0-9]+";
        ConstC = "((\")([A-Za-z0-9\\s].*)(\"))";
        ConstF = Num + "\\." + Num;
        OPA = "([*]|[/]|[%]|[+]|[-]|[DV])";
        Variable = "([A-Z]{1}[a-zA-z]+[0-9]*)";
        AsigE = "(" + Variable + EspIG + Num + ")";
        AsigF = "(" + Variable + EspIG + ConstF + ")";
        AsigC = "(" + Variable + EspIG + ConstC + ")";
        AsigVar = "(" + Variable + EspIG + Variable + ")";
        ConstOva = "(" + ConstC + "|(" + Variable + "))";
        SD = "(SD.DAT[\\s]\\((" + ConstOva + "|" + Num + "|" + ConstF + ")\\)" + finExp + ")";
        ED = "(ED.DAT[\\s]\\((" + Variable + ")\\)" + finExp + ")";
        Comp = "((" + Variable + "|" + Num + ")" + OpRel + "(" + Variable + "|" + Num + "))";
        expAriE = "((" + Variable + "|" + Num + ")" + OPA + "(" + Variable + "|" + Num + "))";
        expAriF = "((" + Variable + "|" + ConstF + ")" + OPA + "(" + Variable + "|" + ConstF + "))";
        expAriC = "(" + ConstOva + "[+]" + ConstOva + ")";
        AsigEXPE = Variable + EspIG + expAriE + finExp;
        AsigEXPF = Variable + EspIG + expAriF + finExp;
        AsigEXPC = Variable + EspIG + expAriC + finExp;
        AsigSimple = AsigE + "|" + AsigF + "|" + AsigC + "|" + AsigVar;
        exp = AsigEXPF + "|" + AsigEXPE + "|" + AsigSimple + "|" + AsigEXPC;
        Si = "Si[\\s][\\:](" + Comp + "|(" + Comp + OpLog + Comp + "))[\\:][\\{]" + finExp + "";
        WH = "WH[\\s]\\:(" + Comp + "|(" + Comp + OpLog + Comp + "))[\\:]" + finExp + "";
        Fr = "FR[\\s]" + Variable + "[\\s]\\:Desde\\((" + Variable + "|" + Num + "|" + expAriE + ")\\)Hasta\\((" + Variable + "|" + Num + "|" + expAriE + ")\\)\\:" + finExp;
        DecaEnt = "(Ent[\\s]" + Variable + finExp + ")|(Ent[\\s]" + AsigE + finExp + ")|(Ent[\\s]" + AsigEXPE + ")" + finExp + "";//VERIFICAR
        DecaStr = "(Stg[\\s]" + Variable + finExp + ")|(Stg[\\s]" + AsigC + finExp + ")|(Stg[\\s]" + AsigEXPC + ")" + finExp + "";
        DecaFlt = "(Flt[\\s]" + Variable + finExp + ")|(Flt[\\s]" + AsigF + finExp + ")|(Flt[\\s]" + AsigEXPF + ")" + finExp + "";
        CiclosFin = "Fin_WH||Fin_For";
    }
    //Se generan los constructores "getter" para poder llamarlos y asi poder utilzarlos para comparar/validar en mi archivo

    public String getNombreVar() {
        return Variable;
    }

    public String getWH() {
        return WH;
    }

    public String getSD() {
        return SD;
    }

    public String getAsigVar() {
        return AsigVar;
    }

    public String getED() {
        return ED;
    }

    public String getSi() {
        return Si;
    }

    public String getNum() {
        return Num;
    }

    public String getFr() {
        return Fr;
    }

    public String getAsigE() {
        return AsigE;
    }

    public String getAsigF() {
        return AsigF;
    }

    public String getAsigC() {
        return AsigC;
    }

    public String getTipos() {
        return Tipo;
    }

    public String getLogicos() {
        return OpLog;
    }

    public String getAritmeticos() {
        return OPA;
    }

    public String getConstOva() {
        return ConstOva;
    }

    public String getDecaEnt() {
        return DecaEnt;
    }

    public String getDecaStr() {
        return DecaStr;
    }

    public String getDecaFlt() {
        return DecaFlt;
    }

    public String getExp() {
        return exp;
    }

    public String getCiclosFin() {
        return CiclosFin;
    }

    public String getAsigEXPE() {
        return AsigEXPE;
    }

}

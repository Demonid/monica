
package analizador;

import java.util.ArrayList;

public class Metodos {

//    public ArrayList<String> getReservadas() {
//        ArrayList<String> reservada = new ArrayList<>();
//        Archivos archivos = new Archivos();
//
//        String reservado = archivos.LeerReservados();
//        for (String palabra : reservado.split("\n")) {
//            reservada.add(palabra);
//        }
//        return reservada;
//    }

    public ArrayList<String> getCodigo() {
        ArrayList<String> reservadas = new ArrayList<>();
        Archivos archivos = new Archivos();

        String reservado = archivos.LeerCodigo();
        for (String palabra : reservado.split("\n")) { //los separa y compara por salto de linea
            reservadas.add(palabra); //cada palabra extraida se almacena en una lista
        }
        return reservadas;
    }
}
